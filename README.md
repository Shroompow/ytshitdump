# Youtube Shit Dump
Shit script for downloading videos from youtube.
Requires [ffmpeg](https://ffmpeg.org) and [youtube-dl](https://github.com/ytdl-org/youtube-dl)
(Script can be run on Windows using the Git Bash. Don't forget to add ffmpeg and youtube-dl to your system PATH)

## Usage
* Full: `ytshitdump URL DEST`
* Section: `ytshitdump URL START_TIME LENGTH DEST`
* Interactive: `ytshitdump -i`
* Show help: `ytshitdump --help`

## Params
* START_TIME, LENGTH: Formatted as `[-][HH:]MM:SS[.m...]` or `[-]S+[.m...]` (<http://ffmpeg.org/ffmpeg-utils.html#time-duration-syntax>)
* DEST: file name (without extension)

## Examples
* `ytshitdump https://www.youtube.com/watch?v=0123456789 "lol hahaha funnnnyyyy"` Downloads full video as "lol hahaha funnnnyyyy.mp4"
* `ytshitdump https://www.youtube.com/watch?v=0123456789 4:10 20 "funnymoment haha"` Downloads video from 4m10s to 4m30s as "funnymoment haha.mp4"

## Troubleshooting
Videos may fail to download if youtube-dl is out of date. Use `youtube-dl -U` to update.