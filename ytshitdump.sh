#!/bin/sh

readonly Scriptname=ytshitdump

show_help()
{
	echo $0
	printf "=== [HELP] ===\n"
	printf "Usage:\n"
	printf "\tFull: \t$Scriptname URL DEST\n"
	printf "\tSection: \t$Scriptname URL START_TIME LENGTH DEST\n"
	printf "\tInteractive: \t$Scriptname -i\n"
	printf "\tShow help: \t$Scriptname --help\n"
	printf "\n"
	printf "START_TIME, LENGTH: Formatted as [-][HH:]MM:SS[.m...] or [-]S+[.m...] (http://ffmpeg.org/ffmpeg-utils.html#time-duration-syntax)\n"
	printf "DEST: file name (without extension)\n"
	printf "\n"
	printf "Examples:\n"
	printf "\t$Scriptname https://www.youtube.com/watch?v=0123456789 \"lol hahaha funnnnyyyy\"\n"
	printf "\t\tDownloads full video as \"lol hahaha funnnnyyyy.mp4\"\n"
	printf "\t$Scriptname https://www.youtube.com/watch?v=0123456789 4:10 20 \"funnymoment haha\"\n"
	printf "\t\tDownloads video from 4m10s to 4m30s \"funnymoment haha.mp4\"\n"
	printf "\n"
	printf "Troubleshooting:\n"
	printf "\tVideos may fail to download if youtube-dl is out of date. Use \"youtube-dl -U\" to update.\n"
}

if [ "$1" == "--help" ]
then
	show_help
	exit
fi

Abort=0

if ! [ -x "$(command -v ffmpeg)"  ];
then
	printf "\e[1;31mWarning: ffmpeg is not installed. ffmpeg is required when downloading sections of a video. Download ffmpeg at https://www.ffmpeg.org/\e[0m\n"
	Ffmpeg=0
else
	Ffmpeg=1
fi

if ! [ -x "$(command -v youtube-dl)"  ];
then
	printf "\e[1;31mError: youtube-dl is not installed. youtube-dl is required to download videos from youtube. More infos at https://github.com/ytdl-org/youtube-dl\e[0m\n"
	Abort=1
fi

if [ $Abort -eq 1 ]
then
	exit 1
fi

if [ "$1" == "-i" ]
then
	read -p "Enter video-url: " Url

	while :
	do
		read -p "Download full video?[y/n]: " Prompt
		if [ "$Prompt" = "y" ]
		then
			Section=0
			break
		elif [ "$Prompt" = "n" ]
		then
			Section=1
			read -p "Enter start-time: " Start
			read -p "Enter length: " Length
			break
		fi
	done
	read -p "Enter destination-path: " Dest
elif [ $# -eq 2 ]
then
	Section=0
	Url=$1
	Dest=$2
elif [ $# -eq 4 ]
then
	Section=1
	Url=$1
	Start=$2
	Length=$3
	Dest=$4
else
	show_help
	exit
fi

if [ $Section -eq 1 ]
then
	if [ $Ffmpeg -ne 1 ]
	then
		printf "\e[1;31mError: Cannot download a section without ffmpeg\e[0m\n"
		exit 1
	fi
	printf "\n"
	printf "Downloading video section...\n"
	printf "\n"
	ffmpeg -ss "$Start" -i $(youtube-dl.exe -f best -g "$Url") -t "$Length" "$Dest.mp4"
else
	printf "\n"
	printf "Downloading video...\n"
	printf "\n"
	youtube-dl.exe -f best "$Url" -o "$Dest.mp4"
fi

if [ $? -eq 0 ]
then
	printf "\n"
	printf "\e[1;32mDownload finished\e[0m\n"
	exit 0
else
	printf "\n"
	printf "\e[1;31mFailed to download\e[0m\n"
	exit $?
fi